We are Dumpster Medic and we have the cheapest Dumpster for rent in town. We’re licensed and insured, offering compact, mini dumpster that are residential friendly. We rent to both businesses and residents throughout the greater Orlando and surrounding areas.

Address: 3162 Dasha Palm Dr, Kissimmee, FL 34744, USA

Phone: 407-516-2657

Website: https://dumpstermedic.com
